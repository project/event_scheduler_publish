<?php

namespace Drupal\event_scheduler_publish;

use Drupal\Core\Entity\ContentEntityBase;

/**
 * Interface SchedulerPublishUtilsInterface
 *
 * @package Drupal\event_scheduler_publish
 */
interface SchedulerPublishUtilsInterface {

  /**
   * Insert the publish/unpublish events for this entity (as appropriate).
   *
   * @param \Drupal\Core\Entity\ContentEntityBase $entity
   *
   * @return void
   */
  public function insertEvents(ContentEntityBase $entity);

  /**
   * Update the publish/unpublish events for this entity (as appropriate).
   *
   * @param \Drupal\Core\Entity\ContentEntityBase $entity
   *
   * @return void
   */
  public function updateEvents(ContentEntityBase $entity);

}
