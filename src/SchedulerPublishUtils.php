<?php

namespace Drupal\event_scheduler_publish;

use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\event_scheduler\EventSchedulerDatabaseInterface;
use Drupal\event_scheduler\EventSchedulerDispatcher;
use Drupal\event_scheduler_publish\Event\EntityPublishEvent;
use Drupal\event_scheduler_publish\Event\EntityScheduledPublishEvent;
use Drupal\event_scheduler_publish\Event\EntityScheduledUnpublishEvent;
use Drupal\event_scheduler_publish\Event\EntityUnpublishEvent;

/**
 * Class SchedulerPublishUtils.
 */
class SchedulerPublishUtils implements SchedulerPublishUtilsInterface {

  /**
   * @var \Drupal\event_scheduler\EventSchedulerDispatcher
   */
  protected $dispatcher;

  /**
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * @var \Drupal\event_scheduler\EventSchedulerDatabaseInterface
   */
  protected $database;

  /**
   * @var \DateTimeZone
   */
  protected $tz;

  /**
   * @var int
   */
  protected $tzOffset = NULL;

  /**
   * Constructs a new SchedulerPublishUtils object.
   *
   * @param \Drupal\event_scheduler\EventSchedulerDispatcher $dispatcher
   * @param \Drupal\event_scheduler\EventSchedulerDatabaseInterface $database
   * @param \Drupal\Core\Logger\LoggerChannelInterface $logger
   */
  public function __construct(
    EventSchedulerDispatcher        $dispatcher,
    EventSchedulerDatabaseInterface $database,
    LoggerChannelInterface          $logger
  ) {
    $this->dispatcher = $dispatcher;
    $this->database = $database;
    $this->logger = $logger;

    $userTimezone = function_exists('drupal_get_user_timezone')
      ? drupal_get_user_timezone()
      : date_default_timezone_get();

    $this->tz = new \DateTimeZone($userTimezone);
  }

  /**
   * @inheritDoc
   *
   * @throws \Exception
   */
  public function insertEvents(ContentEntityBase $entity) {
    $currentTime = (new DrupalDateTime('now', $this->tz))->getTimestamp();
    $entityTag = "{$entity->getEntityTypeId()}:{$entity->id()}";

    // Remove these first, just in case they won't be recreated.
    $this->removeExistingEvent(EntityScheduledPublishEvent::NAME, $entityTag);
    $this->removeExistingEvent(EntityScheduledUnpublishEvent::NAME, $entityTag);

    if ($entity->hasField('field_scheduled_start')
      && !$entity->get('field_scheduled_start')->isEmpty()) {
      // The date field converts and stores the given date & time in UTC.
      // We should set the UTC timezone explicitly, when getting the timestamp,
      // otherwise the default site's timezone is used and time doesn't match.
      $startTime = $this->getTime($entity->get('field_scheduled_start')->value, 'UTC');

      if ($startTime < $currentTime) {
        // The start has already passed, send a publish-entity event now.
        $event = new EntityPublishEvent();
      }
      else {
        // The start time hasn't arrived yet, so schedule it ahead.
        /** @var EntityScheduledPublishEvent $event */
        $event = (new EntityScheduledPublishEvent())
          // Don't use tzAdjust(), timestamps are always in UTC, time gets converted properly.
          ->initialise($startTime, $entityTag);
      }

      $event->setEntityType($entity->getEntityTypeId())
        ->setBundle($entity->bundle())
        ->setEntityId($entity->id());

      $this->dispatcher->dispatch($event::NAME, $event);
    }

    if ($entity->hasField('field_scheduled_end') && !$entity->get('field_scheduled_end')
        ->isEmpty()) {
      $endTime = $this->getTime($entity->get('field_scheduled_end')->value, 'UTC', TRUE);

      if ($endTime < $currentTime) {
        // The end has already passed, send an unpublish-entity event now.
        $this->removeExistingEvent(EntityScheduledPublishEvent::NAME, $entityTag);
        $event = new EntityUnpublishEvent();
      }
      else {
        // The end time hasn't arrived yet, so schedule it ahead.
        /** @var EntityScheduledUnpublishEvent $event */
        $event = (new EntityScheduledUnpublishEvent())
          ->initialise($endTime, $entityTag);
      }

      $event->setEntityType($entity->getEntityTypeId())
        ->setBundle($entity->bundle())
        ->setEntityId($entity->id());

      $this->dispatcher->dispatch($event::NAME, $event);
    }
  }

  /**
   * Get a timestamp from what we are given.
   *
   * If it's midnight and the end time, make it the end of the day.
   *
   * @param string $timeValue
   *
   * @param mixed $timezone
   *
   * @param bool $isEnd
   *
   * @return int
   */
  protected function getTime(string $timeValue, $timezone = NULL, $isEnd = FALSE): int {
    // Get the date/time object adjusting for timezone;
    $timestamp = (new DrupalDateTime($timeValue, $timezone))->getTimestamp();

    // Is this exactly midnight?
    if (($timestamp % 86400) === 0) {
      if ($isEnd) {
        // If it's the end time, set it to one second to midnight.
        $timestamp += 86399;
      }
    }

    return $timestamp;
  }

  /**
   * Get the time offset in seconds.
   *
   * Counter intuitive but we need double the amount to compensate.
   *
   * @return int
   *
   * @throws \Exception
   */
  protected function tzAdjust(): int {
    if ($this->tzOffset === NULL) {
      $defaultDateTime = new \DateTime();
      $this->tzOffset = $this->tz->getOffset($defaultDateTime);
    }
    return $this->tzOffset * 2;
  }

  /**
   * Remove a matching event from the event scheduler database.
   *
   * @param string $eventName
   * @param string $entityTag
   */
  protected function removeExistingEvent(string $eventName, string $entityTag): void {
    $this->database->delete([
      'name' => ['value' => $eventName],
      'tag' => ['value' => $entityTag],
    ]);
  }

  /**
   * @inheritDoc
   */
  public function updateEvents(ContentEntityBase $entity) {
    /** @var ContentEntityBase $original */
    $original = $entity->original;
    $changed = FALSE;

    if ($entity->hasField('field_scheduled_start')
      && !$entity->get('field_scheduled_start')->isEmpty()) {
      if ($original->hasField('field_scheduled_start')
        && !$original->get('field_scheduled_start')->isEmpty()) {
        $changed = $entity->get('field_scheduled_start')->value !== $original->get('field_scheduled_start')->value;
      }
      else {
        // The start field has just been used - that's a change.
        $changed = TRUE;
      }
    }
    elseif ($original->hasField('field_scheduled_start')) {
      // The start field has been removed, so that's a change too.
      $changed = TRUE;
    }

    if (!$changed && $entity->hasField('field_scheduled_end')
      && !$entity->get('field_scheduled_end')->isEmpty()) {
      if ($original->hasField('field_scheduled_end')
        && !$original->get('field_scheduled_end')->isEmpty()) {
        $changed = $entity->get('field_scheduled_end')->value !== $original->get('field_scheduled_end')->value;
      }
      else {
        // The end field has just been used - that's a change.
        $changed = TRUE;
      }
    }
    elseif ($original->hasField('field_scheduled_end')) {
      // The end field has been removed, so that's a change too.
      $changed = TRUE;
    }

    if ($changed) {
      $this->insertEvents($entity);
    }
  }

}
