<?php

namespace Drupal\event_scheduler_publish\EventSubscriber;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\event_scheduler\EventSchedulerDispatcher;
use Drupal\event_scheduler_publish\Event\EntityPublishedEvent;
use Drupal\event_scheduler_publish\Event\EntityPublishEvent;
use Drupal\event_scheduler_publish\Event\EntityPublishEventInterface;
use Drupal\event_scheduler_publish\Event\EntityScheduledPublishEvent;
use Drupal\event_scheduler_publish\Event\EntityScheduledUnpublishEvent;
use Drupal\event_scheduler_publish\Event\EntityUnpublishedEvent;
use Drupal\event_scheduler_publish\Event\EntityUnpublishEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Class EntityPublishSubscriber
 *
 * @package Drupal\event_scheduler_publish\EventSubscriber
 */
class EntityPublishSubscriber implements EventSubscriberInterface {

  /**
   * @var EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * @var EventSchedulerDispatcher
   */
  protected $dispatcher;

  /**
   * Constructs a new EntityPublishSubscriber object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   * @param EventSchedulerDispatcher $dispatcher
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, EventSchedulerDispatcher $dispatcher) {
    $this->entityTypeManager = $entity_type_manager;
    $this->dispatcher = $dispatcher;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[EntityPublishEvent::NAME] = ['onEntityPublish'];
    $events[EntityScheduledPublishEvent::NAME] = ['onEntityPublish'];
    $events[EntityUnpublishEvent::NAME] = ['onEntityUnpublish'];
    $events[EntityScheduledUnpublishEvent::NAME] = ['onEntityUnpublish'];

    return $events;
  }

  /**
   * Called when an entity is to be unpublished.
   *
   * With the delayed event there's a possibility the entity no longer
   * exists, which is why we check. And we don't bother unpublishing
   * if it's already unpublished.
   *
   * @param EntityPublishEventInterface $event
   *   The dispatched event.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function onEntityPublish(EntityPublishEventInterface $event) {
    $storage = $this->entityTypeManager->getStorage($event->getEntityType());

    /** @var ContentEntityBase $entity */
    $entity = $storage->load($event->getEntityId());

    // Entity exists and is not published...
    if ($entity && $entity->hasField('status') && !$entity->get('status')->value) {
      $entity->set('status', 1);
      $entity->save();

      $publishedEvent = new EntityPublishedEvent();

      $publishedEvent->setEntityType($entity->getEntityTypeId())
            ->setBundle($entity->bundle())
            ->setEntityId($entity->id());

      $this->dispatcher->dispatch($publishedEvent::NAME, $publishedEvent);
    }
  }

  /**
   * Called when an entity is to be unpublished.
   *
   * With the delayed event there's a possibility the entity no longer
   * exists, which is why we check. And we don't bother unpublishing
   * if it's already unpublished.
   *
   * @param EntityPublishEventInterface $event
   *   The dispatched event.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function onEntityUnpublish(EntityPublishEventInterface $event) {
    $storage = $this->entityTypeManager->getStorage($event->getEntityType());

    /** @var ContentEntityBase $entity */
    $entity = $storage->load($event->getEntityId());

    // Entity exists and is published...
    if ($entity && $entity->hasField('status') && $entity->get('status')->value) {
      $entity->set('status', 0);
      $entity->save();

      $unpublishedEvent = new EntityUnpublishedEvent();

      $unpublishedEvent->setEntityType($entity->getEntityTypeId())
                     ->setBundle($entity->bundle())
                     ->setEntityId($entity->id());

      $this->dispatcher->dispatch($unpublishedEvent::NAME, $unpublishedEvent);
    }
  }

}
