<?php

namespace Drupal\event_scheduler_publish\Event;

use Drupal\event_scheduler\Event\EventDelayInterface;

/**
 * Class EntityPublishedEvent
 *
 * @package Drupal\event_scheduler_publish\Event
 */
class EntityPublishedEvent extends EntityPublishEventBase implements EventDelayInterface {

  const NAME = 'event_scheduler_publish.published';

}
