<?php

namespace Drupal\event_scheduler_publish\Event;

use Drupal\Component\EventDispatcher\Event;

/**
 * Class EntityPublishEventBase
 *
 * @package Drupal\event_scheduler_publish\Event
 */
abstract class EntityPublishEventBase extends Event implements EntityPublishEventInterface {

  const NAME = 'event_scheduler_publish.abstract';

  /**
   * @var string
   */
  protected $entityType;

  /**
   * @var string
   */
  protected $bundle;

  /**
   * @var string
   */
  protected $entityId;

  /**
   * @param string $entityType
   *
   * @return EntityPublishEventBase
   */
  public function setEntityType(string $entityType): EntityPublishEventBase {
    $this->entityType = $entityType;
    return $this;
  }

  /**
   * @param string $bundle
   *
   * @return EntityPublishEventBase
   */
  public function setBundle(string $bundle): EntityPublishEventBase {
    $this->bundle = $bundle;
    return $this;
  }

  /**
   * @param string $entityId
   *
   * @return EntityPublishEventBase
   */
  public function setEntityId(string $entityId): EntityPublishEventBase {
    $this->entityId = $entityId;
    return $this;
  }

  /**
   * @return string
   */
  public function getEntityType(): string {
    return $this->entityType;
  }

  /**
   * @return string
   */
  public function getBundle(): string {
    return $this->bundle;
  }

  /**
   * @return string
   */
  public function getEntityId(): string {
    return $this->entityId;
  }

  /**
   * @return string
   */
  public function entityTag(): string {
    return "{$this->getEntityType()}:{$this->getEntityId()}";
  }

}
