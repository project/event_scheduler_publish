<?php


namespace Drupal\event_scheduler_publish\Event;

/**
 * Interface EntityPublishEventInterface
 *
 * @package Drupal\event_scheduler_publish\Event
 */
interface EntityPublishEventInterface {

  /**
   * @param string $entityType
   *
   * @return EntityPublishEventBase
   */
  public function setEntityType(string $entityType): EntityPublishEventBase;

  /**
   * @param string $bundle
   *
   * @return EntityPublishEventBase
   */
  public function setBundle(string $bundle): EntityPublishEventBase;

  /**
   * @param string $entityId
   *
   * @return EntityPublishEventBase
   */
  public function setEntityId(string $entityId): EntityPublishEventBase;

  /**
   * @return string
   */
  public function getEntityType(): string;

  /**
   * @return string
   */
  public function getBundle(): string;

  /**
   * @return string
   */
  public function getEntityId(): string;

  /**
   * @return string
   */
  public function entityTag(): string;

}
