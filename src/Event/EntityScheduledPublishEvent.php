<?php

namespace Drupal\event_scheduler_publish\Event;

use Drupal\event_scheduler\Event\EventScheduleInterface;
use Drupal\event_scheduler\EventSchedulerTrait;

/**
 * Class EntityScheduledPublishEvent
 *
 * @package Drupal\event_scheduler_publish\Event
 */
class EntityScheduledPublishEvent extends EntityPublishEventBase implements EventScheduleInterface {

  use EventSchedulerTrait;

  const NAME = 'event_scheduler_publish.scheduled_publish';

}
