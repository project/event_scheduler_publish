<?php

namespace Drupal\event_scheduler_publish\Event;

use Drupal\event_scheduler\Event\EventDelayInterface;

/**
 * Class EntityPublishEvent
 *
 * @package Drupal\event_scheduler_publish\Event
 */
class EntityPublishEvent extends EntityPublishEventBase implements EventDelayInterface {

  const NAME = 'event_scheduler_publish.publish';

}
