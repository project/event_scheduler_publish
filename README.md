Event Scheduler Publish
=======================

INTRODUCTION
------------
This module leverages the Event Scheduler module to provide publish and
unpublish events for any content entity that has a standard "status" field.

### Fields

Once enabled there is nothing much to do except that the entity in question
must have either or both of the following fields:

* `field_scheduled_start`
* `field_scheduled_end`

These must be date fields storing in the `Y-m-d\TH:i:s` format.

When a date is given but no time, it will be assumed to be the start of the day
for the publish, and the end of the day for the unpublish.

The timezone used will be the one belonging to the creator of the entity.

### Events

It uses the following events to indicate the action to be performed:

* `EntityPublishEvent` Publish now.
* `EntityUnpublishEvent` Unpublish now.
* `EntityScheduledPublishEvent` Publish some time in the future.
* `EntityScheduledUnpublishEvent` Unpublish some time in the future.

And these two events are issued once the action has been performed:

* `EntityPublishedEvent` when an entity has been successfully published.
* `EntityUnpublishedEvent` when an entity has been successfully unpublished.

It's important to understand the first group of events occur before the action
has been performed, and the other two after the action has successfully been
performed. If the publish/unpublish action was unsuccessful the relevant event
will not be issued.

If you want to carry out other actions you will normally intercept those two in
an event subscriber. Those two events are both delayed events and will be
launched after the rest of the page has been built and sent to the browser.

You can get the entity type, bundle and entity ID using these three event
methods:

* `getEntityType()`
* `getBundle()`
* `getEntityId()`

INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. Visit
   https://www.drupal.org/node/1897420 for further information.

CONFIGURATION
-------------
This module does not require configuration.

